local lspconfig = require 'lspconfig'

local on_lsp_attach = function (client, bufnr)
    local function lnmap(...) 
        vim.api.nvim_buf_set_keymap(bufnr, 'n', ...)
    end
    local opts = { noremap = true, silent = true }

    lnmap('gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
    lnmap('gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)

    lnmap('<leader>ln', '<Cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
    lnmap('<leader>lp', '<Cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)

    lnmap('<leader>lr', '<Cmd>lua vim.lsp.buf.rename()<CR>', opts)
    lnmap('<leader>lf', '<Cmd>lua vim.lsp.buf.formatting()<CR>', opts)
    
    if client.resolved_capabilities.document_hightlight then
        vim.api.nvim_exec([[
            hi LspReferenceRead cterm=bold ctermbg=red guibg=LightYellow
            hi LspReferenceText cterm=bold ctermbg=red guibg=LightYellow
            hi LspReferenceWrite cterm=bold ctermbg=red guibg=LightYellow
            augroup lsp_document_highlight
                autocmd! * <buffer>
                autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
                autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
            augroup END
        ]])
    end
end

local has_words_before = function()
    local line, col = unpack(vim.api.nvim_win_get_cursor(0))
    return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local feedkey = function(key, mode)
    vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes(key, true, true, true), mode, true)
end

local cmp = require 'cmp'
cmp.setup({
    snippet = {
    },
    mapping = {
      ['<C-d>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
      ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
      ['<Tab>'] = cmp.mapping(function (fallback) 
          if cmp.visible() then
              cmp.select_next_item()
          elseif has_words_before() then
              cmp.complete()
          else 
              fallback()
          end
      end, { 'i', }),
      ["<S-Tab>"] = cmp.mapping(function()
          if cmp.visible() then
              cmp.select_prev_item()
          end
      end, { "i", }),
      ['<C-y>'] = cmp.config.disable,
      ['<C-e>'] = cmp.mapping({
        i = cmp.mapping.abort(),
        c = cmp.mapping.close(),
      }),
      ['<CR>'] = cmp.mapping.confirm({ select = true }),
    },
    sources = cmp.config.sources({
      { name = 'nvim_lsp' },
    }, {
      { name = 'buffer' },
    }),
    completion = {
        keyword_length = 3,
    },
    view = {
        entries = 'native',
    },
})

local cmp_nvim_lsp = require 'cmp_nvim_lsp'
local capabilities = cmp_nvim_lsp.default_capabilities

lspconfig['rls'].setup {
    on_attach = on_lsp_attach,
    capabilities = capabilities,
}

lspconfig['clangd'].setup {
    on_attach = on_lsp_attach,
    capabilities = capabilities,
    cmd = { "clangd", "--background-index", "--compile-commands-dir=build" },
}

lspconfig['cmake'].setup {
    on_attach = on_lsp_attach,
    capabilities = capabilities,
}
