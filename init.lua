local is_packer_sync_needed = false
do
    local packer_path = vim.fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
    if vim.fn.empty(vim.fn.glob(packer_path)) > 0 then
        is_packer_sync_needed = true
        vim.fn.system({'git', 'clone', 'https://github.com/wbthomason/packer.nvim', packer_path})
        vim.fn.execute 'packadd packer.nvim'
    end
end

local packer = require 'packer'
packer.startup(function(use)
    use 'wbthomason/packer.nvim'

    use {
        'navarasu/onedark.nvim',
        config = function()
            require('onedark').load()
        end,
    }

    use {
        'haystackandroid/strawberry',
        config = function()
            vim.cmd [[colorscheme strawberry-light]]
        end,
        disable = true,
    }

    use 'editorconfig/editorconfig-vim'

    use {
        'numToStr/Comment.nvim',
        config = function()
            require('Comment').setup {
                toggler = {
                    line = '<leader>;',
                },
                opleader = {
                    line = "<leader>;",
                },
                mappings = {
                    basic = true,
                },
                ignore = "^ *$",
            }
        end,
    }

    use 'tpope/vim-surround'
    use 'jreybert/vimagit'
    use 'lyokha/vim-xkbswitch'
    use 'airblade/vim-gitgutter'

    use 'jceb/vim-orgmode'
    use 'PProvost/vim-ps1'

    use 'preservim/nerdtree'
    use 'lambdalisue/suda.vim'

    use 'neovim/nvim-lspconfig'
    use 'nvim-treesitter/nvim-treesitter'

    use 'hrsh7th/cmp-nvim-lsp'
    use 'hrsh7th/cmp-buffer'
    use 'hrsh7th/cmp-cmdline'
    use 'hrsh7th/nvim-cmp'

    if is_packer_sync_needed then
        packer.sync()
    end
end)

vim.o.number = true
vim.o.guifont = 'Cascadia Mono:h12'

vim.o.expandtab = true
vim.o.tabstop = 4
vim.o.shiftwidth = 4
vim.o.smartindent = true
vim.o.breakindent = true
vim.o.backspace = 'indent,eol,start'

vim.o.wildmenu = true
vim.o.wildmode = 'longest,list,full'

vim.o.showmatch = true
vim.o.incsearch = true
vim.o.hlsearch = true

vim.o.lazyredraw = true
vim.o.scrolloff = 4
vim.o.ruler = true
vim.o.errorbells = false
vim.o.visualbell = true

vim.o.pastetoggle = '<f2>'
vim.o.autoread = true
vim.o.autochdir = true
vim.o.autoread = true
vim.o.autowriteall = true
vim.o.confirm = true

vim.o.completeopt = 'menu,menuone,noselect'

local function map(mode, lhs, rhs, opts)
    local options = {noremap = true}
    if opts then 
        options = vim.tbl_extend('force', options, opts) 
    end
    vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

local function nmap(lhs, rhs, opts)
    return map('n', lhs, rhs, opts)
end

local function mapl(lhs, rhs, opts)
    return map('', '<leader>' .. lhs, rhs, opts)
end

nmap('j', 'gj', nil)
nmap('k', 'gk')
nmap('gV', '`[v`]')

vim.g.mapleader = ' '
mapl('fs', ':w<cr>')
-- mapl('mm', ':make<cr>')
mapl('<tab>', 'g<tab>')
mapl('ff', ':tabedit ')
mapl("bd", ':tabclose<cr>')
mapl('qq', ':wq<cr>')
mapl('/', ':nohlsearch<cr>', {silent = true})
mapl('fr', ':browse oldfiles<cr>', {silent = true})

mapl('y', '"+y')
mapl('p', '"+p')

for i = 1, 9 do
    local nr = tostring(i)
    mapl(nr, ':tabn '..nr..'<cr>')
end

mapl("fo", ":GonvimFilerOpen<cr>")
mapl("mm", ":GonvimMiniMap<cr>")

mapl("wn", ":GonvimWorkspaceNew<cr>")
mapl("wj", ":GonvimWorkspaceNext<cr>")
mapl("wk", ":GonvimWorkspacePrevious<cr>")
for i = 1, 9 do
    mapl("w"..tostring(i), ":GnvimWorkspaceSwitch "..tostring(i).."<cr>")
end

mapl('ft', ':NERDTreeToggle<cr>', {silent = true})
mapl('fd', ':NERDTreeFocus<cr>', {silent = true})

vim.o.showtabline = 0

vim.o.signcolumn = 'yes'

vim.g.suda_smart_edit = 1

vim.cmd 'filetype plugin indent on'
vim.cmd 'syntax enable'

require 'my.lspconf'
require 'my.treesitter'

